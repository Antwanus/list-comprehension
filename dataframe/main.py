student_dict = {
    "student": ["An", "Billy", "Charly", "Dick", "Eric"],
    "score": [56, 76, 98, 82, 71]
}
for (key, value) in student_dict.items():
    print(value)

import pandas

student_df = pandas.DataFrame(student_dict)

# LOOP THROUGH DATAFRAME
# for (k, v) in student_df.items():
# #       print(k)
#     print(v)

# LOOP THROUGH ROWS OF A DATAFRAME
for(index, row) in student_df.iterrows():
    # print(index)
    # print(row)
    if row.student == "An":
        print(row.student)

