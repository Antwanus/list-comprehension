import pandas

data = pandas.read_csv("nato_abc.csv")

phonetic_dict = {row.letter: row.code for (index, row) in data.iterrows()}
print(phonetic_dict)


def generate_nato_phonetic():
    input_word = input("Enter a word: ").upper()
    try:
        output_list = [phonetic_dict[letter] for letter in input_word]
    except KeyError:
        print("only letter from the alphabet please!")
        generate_nato_phonetic()
    else:
        print(output_list)


generate_nato_phonetic()
