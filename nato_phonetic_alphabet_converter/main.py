import pandas

student_dict = {
    "student": ["An", "Billy", "Charly", "Dick", "Eric"],
    "score": [56, 76, 98, 82, 71]
}
student_df = pandas.DataFrame(student_dict)
nato_df = pandas.read_csv("nato_abc.csv")

# create dict where 'A': 'Alpha,...
d = {row["letter"]: row["code"] for (index, row) in nato_df.iterrows()}
print(d)


def name_to_nato_abc_list(name):
    """ :returns a list of all uppercase characters from a string :param name"""
    return [d[char.upper()] for char in name]


student_nato_dict = {student: name_to_nato_abc_list(student) for student in student_df["student"]}
print(student_nato_dict)
