numbers = [1, 2, 3, 4]
new_list = [(n + 1) for n in numbers]

name = "Antoon"
letters_list = [char for char in name]

range_list = [i * 2 for i in range(1, 5)]

names_list = ["Antoon", "Barrack", "Charlie", "Pif", "Paf", "Pef"]
short_names_list = [n for n in names_list if len(n) < 5]
