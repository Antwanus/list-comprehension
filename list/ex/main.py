# numbers = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]
#
# # EX1
# squared_numbers = [n ** 2 for n in numbers]
#
# print(squared_numbers)
#
# # EX2
# even_numbers = [n for n in numbers if n % 2 == 0]
#
# print(even_numbers)

# EX3
# TODO compare data between file1 & file2.txt, create a list of numbers that are common
f1 = open("file1.txt")
f1_lines = f1.readlines()
f1.close()
f1_as_list = [int(line) for line in f1_lines]

f2 = open("file2.txt")
f2_lines = f2.readlines()
f2.close()
f2_as_list = [int(line) for line in f2_lines]


result = [n for n in f1_as_list if n in f2_as_list]
print(result)