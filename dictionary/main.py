# new dict = {new_key:new_value for item in list}
# new_dict = {new_key:new_value for (key, value) in dict.items()}
# new_dict = {new_key:new_value for (key, value) in dict.items() if predicate}

names_list = ['Alex', 'Baldrick', 'Charles', 'Dick', 'Edward']
names_dict = {item: len(item) for item in names_list if len(item) < 5}
print(names_dict)

weather_c = {
    "Monday": 12,
    "Tuesday": 14,
    "Wednesday": 15,
    "Thursday": 14,
    "Friday": 21,
    "Saturday": 22,
    "Sunday": 24,
}


def celsius_to_fahrenheit(temp):
    return temp * 9 / 5 + 32


weather_fahrenheit = {key: celsius_to_fahrenheit(value) for (key, value) in weather_c.items()}
print(weather_fahrenheit)


